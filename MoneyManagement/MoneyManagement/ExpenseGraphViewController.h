//
//  ExpenseGraphViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BaseChartViewController.h"

@interface ExpenseGraphViewController : BaseChartViewController

@property (nonatomic, strong) NSArray<ExpenseEvent *> *arrayExpense;

@end
