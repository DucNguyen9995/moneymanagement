//
//  IncomeAddViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/5/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "BaseViewController.h"
#import <BEMCheckBox/BEMCheckBox.h>

@interface IncomeAddViewController : BaseViewController<BEMCheckBoxDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnCreate;
@property (weak, nonatomic) IBOutlet UITextField *textFieldMoneyAmt;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDesc;
@property (weak, nonatomic) IBOutlet UIButton *btnOptionalBankCash;
@property (weak, nonatomic) IBOutlet UIButton *btnDate;
@property (weak, nonatomic) IBOutlet BEMCheckBox *checkOneTime;
@property (weak, nonatomic) IBOutlet BEMCheckBox *checkMonthly;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

- (IBAction)btnBackDidTap:(id)sender;
- (IBAction)btnDateDidTap:(id)sender;

@end
