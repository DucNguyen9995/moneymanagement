//
//  BalanceGraphViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BalanceGraphViewController.h"
#import "BalanceViewController.h"
#import <Masonry/Masonry.h>

#define kLabelBannerDesc                        @"Total: $%@"

@interface BalanceGraphViewController ()

@end

@implementation BalanceGraphViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dictBank = [[NSMutableDictionary alloc] init];
    NSArray<Bank *> *banks = [CommonUtils fetchBanks];
    Bank *cash = [banks objectAtIndex:0];
    banks = [banks subarrayWithRange:NSMakeRange(1, [banks count] - 1)];
    for(Bank *bank in banks) {
        [_dictBank setValue:[NSNumber numberWithFloat:bank.amount] forKey:bank.name];
    }
    _cash = cash.amount;
    self.legendOn = YES;
    self.ringSize = 5000;
    self.legendStyle = PNLegendItemStyleSerial;
    [self loadGraph];
    [self displayDescriptionWithPercent:[NSString stringWithFormat:@"%ld", [self calculateTotal]]];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadGraph {
    NSMutableArray<NSNumber *> *itemValues = [[NSMutableArray alloc] init];
    [itemValues addObject:[NSNumber numberWithFloat:_cash]];
    [itemValues addObject:[NSNumber numberWithFloat:[self calculateTotal]]];
    NSMutableArray<UIColor *> *itemColors = [[NSMutableArray alloc] init];
    [itemColors addObject:PNLightGreen];
    [itemColors addObject:PNRed];
    [self loadDataForPieChartItemValues:itemValues
                                 colors:itemColors
                           descriptions:[NSMutableArray arrayWithObjects:@"Cash", @"Bank", nil]];
}

- (NSInteger)calculateTotal {
    NSInteger  total = _cash;
    for(id key in _dictBank) {
        total += [[_dictBank objectForKey:key] integerValue];
    }
    return total;
}

- (void)displayDescriptionWithPercent:(NSString *)moneyAmtStr {
    UILabel *lblDesc = [[UILabel alloc] init];
    [lblDesc setTextAlignment:NSTextAlignmentCenter];
    [lblDesc setText:[NSString stringWithFormat:kLabelBannerDesc, moneyAmtStr]];
    [self.view addSubview:lblDesc];
    [lblDesc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pieChart.mas_bottom).with.offset(0);
        make.bottom.equalTo(self.view.mas_bottom).with.offset(0);
        make.right.equalTo(self.pieChart.mas_right).with.offset(0);
        make.left.equalTo(self.pieChart.mas_left).with.offset(0);
    }];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    BalanceViewController *vc = [[BalanceViewController alloc] initWithNibName:@"BalanceViewController" bundle:[NSBundle mainBundle]];
    vc.dictBanks = _dictBank;
    vc.currentCash = _cash;
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
