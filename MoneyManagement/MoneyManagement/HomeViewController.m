//
//  HomeViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/20/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "HomeViewController.h"
#import "BaseChartViewController.h"
#import "RatioGraphViewController.h"
#import "BalanceGraphViewController.h"
#import "IncomeGraphViewController.h"
#import "ExpenseGraphViewController.h"
#import "MTUser.h"

#define kCarbonNavigationColorDefault      [UIColor redColor]


@interface HomeViewController () <CarbonTabSwipeNavigationDelegate> {
    NSArray *viewControllers;
    CarbonTabSwipeNavigation *carbonNavigation;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Home";
    viewControllers = @[
        @"Ratio",
        @"Balance",
        @"Income",
        @"Expense"
    ];
    carbonNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:viewControllers delegate:self];
    [carbonNavigation insertIntoRootViewController:self];
    [self style];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)style {
    carbonNavigation.toolbar.translucent = NO;
    [carbonNavigation setIndicatorColor:kCarbonNavigationColorDefault];
    [carbonNavigation setTabExtraWidth:([[UIScreen mainScreen] bounds].size.width - [carbonNavigation.carbonSegmentedControl getWidth]) / 8];
}

#pragma mark - Carbon Navigator Delegate
// Required
- (UIViewController *)carbonTabSwipeNavigation:(CarbonTabSwipeNavigation *)carbonTabSwipeNavigation viewControllerAtIndex:(NSUInteger)index {
    BaseChartViewController *vc = nil;
    NSMutableArray *temp;
    switch (index) {
        case 0:
            vc = nil;
            vc = [[RatioGraphViewController alloc] initWithNibName:@"RatioGraphViewController" bundle:[NSBundle mainBundle]];
            ((RatioGraphViewController *)vc).spentMoney = [[MTUser sharedInstance] spentMoneyOfThisMonth];
            ((RatioGraphViewController *)vc).allMoney = [[[CommonUtils fetchBanks] objectAtIndex:0] amount];
            vc.showOnlyValue = NO;
            break;
        case 1:
            vc = nil;
            vc = [[BalanceGraphViewController alloc] init];
            vc.showOnlyValue = NO;
            break;
        case 2:
            vc = [[IncomeGraphViewController alloc] init];
            temp = [[NSMutableArray alloc] init];
            for (IncomeEvent *evt in [CommonUtils fetchTestArrayIncome:30]) {
                if(!evt.active) {
                    [temp addObject:[NSNumber numberWithInteger:evt.amount]];
                }
            }
            ((IncomeGraphViewController *)vc).expectedMoney = [temp copy];
            for (IncomeEvent *evt in [CommonUtils fetchTestArrayIncome:30]) {
                if(evt.active) {
                    [temp addObject:[NSNumber numberWithInteger:evt.amount]];
                }
            }
            ((IncomeGraphViewController *)vc).unexpectedMoney = [temp copy];
            vc.legendOn = YES;
            vc.showOnlyValue = NO;
            break;
        case 3:
            vc = [[ExpenseGraphViewController alloc] init];
            vc.ringSize = 5000;
            ((ExpenseGraphViewController *)vc).arrayExpense = [CommonUtils fetchTestArrayExpense:10];
            vc.legendOn = YES;
            vc.showOnlyValue = YES;
            break;
            
        default:
            return vc = [[BaseChartViewController alloc] init];
    }
    return vc;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
