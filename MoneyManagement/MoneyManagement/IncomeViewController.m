//
//  IncomeViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/28/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "IncomeViewController.h"
#import "IncomeAddViewController.h"
#import "BaseEventViewController.h"

@interface IncomeViewController () {
    NSArray<NSString *> *cellLabels;
}

@end

@implementation IncomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Income";
    cellLabels = @[@"Monthly Income", @"Daily Income", @"", @"Today Income"];
    _tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_tblView setDataSource:self];
    [_tblView setDelegate:self];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [cellLabels count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIncomeAddID];
    if(cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIncomeAddID];
    [cell.textLabel setText:[cellLabels objectAtIndex:indexPath.row]];
    if([[cellLabels objectAtIndex:indexPath.row] isEqualToString:@""])
        cell.accessoryType = UITableViewCellAccessoryNone;
    else
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    BaseViewController *vc;
    switch (indexPath.row) {
        case 0:
            vc = [[BaseEventViewController alloc] initWithNibName:@"BaseEventViewController" bundle:[NSBundle mainBundle]];
            ((BaseEventViewController *)vc).events = [CommonUtils fetchTestArrayIncome:50];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        case 1:
            vc = [[BaseEventViewController alloc] initWithNibName:@"BaseEventViewController" bundle:[NSBundle mainBundle]];
            ((BaseEventViewController *)vc).events = [CommonUtils fetchTestArrayIncome:10];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        case 3:
            vc = [[IncomeAddViewController alloc] initWithNibName:@"IncomeAddViewController" bundle:[NSBundle mainBundle]];
            [self presentViewController:vc animated:YES completion:nil];
            break;
            
        default:
            break;
    }
    if(vc != nil)
        vc.title = [cellLabels objectAtIndex:indexPath.row];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
