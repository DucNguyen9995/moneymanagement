//
//  ValueTableViewCell.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "ValueTableViewCell.h"

@implementation ValueTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
