//
//  GraphTableViewCell.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/26/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BEMSimpleLineGraph/BEMSimpleLineGraphView.h>

#define kCellGraphId                @"GraphCell"

@interface GraphTableViewCell : UITableViewCell<BEMSimpleLineGraphDataSource>

@property (nonatomic, strong) BEMSimpleLineGraphView *graphView;

@property (nonatomic, strong) NSMutableArray<NSNumber *> *arrayOfYAxis;
@property (nonatomic, strong) NSMutableArray *arrayOfXAxis;

- (instancetype)initWithYAxisData:(NSMutableArray<NSNumber *> *)yAxis
                        xAxisData:(NSMutableArray *)xAxis;

@end
