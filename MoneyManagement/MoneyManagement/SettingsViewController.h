//
//  SettingsViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/23/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BaseViewController.h"
#import <VenTouchLock/VENTouchLock.h>

#define kCellSettingsID         @"kCellSettings"
#define kAppleAppLink           @"https://www.google.com"
#define kFacebookAppLink        @"fb://bach.kinh.xay.confession"
#define kFacebookLink           @"http://facebook.com/bach.kinh.xay.confession"

@interface SettingsViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblSettings;

@end
