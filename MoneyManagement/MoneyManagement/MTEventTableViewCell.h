//
//  MTEventTableViewCell.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/13/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BEMCheckBox/BEMCheckBox.h>

#define kCellEventIdentifier        @"kCellEventIdentifier"

@protocol MTEventTableViewCellDelegate <NSObject>

@required
- (void)didTapCheckBox:(BEMCheckBox *)checkBox;
- (void)executeDeleteCellWithBtn:(UIButton *)btn;

@end

@interface MTEventTableViewCell : UITableViewCell<BEMCheckBoxDelegate>
@property (weak, nonatomic) IBOutlet BEMCheckBox *checkBox;
@property (weak, nonatomic) IBOutlet UILabel *lblEventName;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet id <MTEventTableViewCellDelegate> delegate;
- (IBAction)btnDeleteDidTap:(id)sender;

@end
