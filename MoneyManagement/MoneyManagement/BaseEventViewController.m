//
//  BaseEventViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/13/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "BaseEventViewController.h"

@interface BaseEventViewController () {
    
}

@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end

@implementation BaseEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareUI];
    [self prepareData];
    // Do any additional setup after loading the view from its nib.
}

- (void)prepareData {
    [_tblView setDelegate:self];
    [_tblView setDataSource:self];
}

- (void)prepareUI {
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_events count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView registerClass:[MTEventTableViewCell class] forCellReuseIdentifier:kCellEventIdentifier];
    [tableView registerNib:[UINib nibWithNibName:@"MTEventTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCellEventIdentifier];
    MTEventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellEventIdentifier];
    if(!cell)
        cell = [[MTEventTableViewCell alloc] init];
    MoneyEvent *evt = [_events objectAtIndex:indexPath.row];
    cell.delegate = self;
    [cell.lblEventName setText:[evt desc]];
    [cell.lblAmount setText:[NSString stringWithFormat:@"$%d",[evt amount]]];
    CGRect rect = [cell.btnDelete frame];
    [cell.btnDelete setImage:[CommonUtils resizeImage:[UIImage imageNamed:@"trash_icon"]
                                           toFitWidth:rect.size.width
                                               height:rect.size.width]
                    forState:UIControlStateNormal & UIControlStateSelected];
    if(evt.active)
        [cell.checkBox setOn:[evt active] animated:YES];
    else
        [cell.checkBox setOn:[evt active] animated:NO];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MTEventTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell.delegate didTapCheckBox:cell.checkBox];
    [cell.checkBox setOn:!cell.checkBox.on animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)executeDeleteCellWithBtn:(UIButton *)btn {
    CGPoint point = [btn convertPoint:CGPointZero toView:_tblView];
    NSIndexPath *indexPath = [_tblView indexPathForRowAtPoint:point];
    NSMutableArray *temp = [NSMutableArray arrayWithArray:_events];
    [temp removeObjectAtIndex:indexPath.row];
    _events = [NSArray arrayWithArray:temp];
    [_tblView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
    NSLog(@"Delete cell number %ld", indexPath.row);
}

- (void)didTapCheckBox:(BEMCheckBox *)checkBox {
    CGPoint point = [checkBox convertPoint:CGPointZero toView:_tblView];
    NSIndexPath *indexPath= [_tblView indexPathForRowAtPoint:point];
    [_events objectAtIndex:indexPath.row].active = ![_events objectAtIndex:indexPath.row].active;
    NSLog(@"Tap in TblView");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
