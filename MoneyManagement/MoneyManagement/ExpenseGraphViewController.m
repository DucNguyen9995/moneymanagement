//
//  ExpenseGraphViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "ExpenseGraphViewController.h"
#import "TimelineViewController.h"

@interface ExpenseGraphViewController () {
    NSMutableArray<NSString *> *itemNames;
    NSMutableArray<NSNumber *> *itemValues;
    NSMutableArray<UIColor *> *itemColors;
}

@end

@implementation ExpenseGraphViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    itemColors = [[NSMutableArray alloc] init];
    [itemColors addObject:PNLightGreen];
    [itemColors addObject:PNRed];
    [itemColors addObject:PNLightBlue];
    [itemColors addObject:PNPinkGrey];
    [itemColors addObject:PNStarYellow];
    [self rearrangeData];
    [self loadGraph];
    // Do any additional setup after loading the view.
}

- (void)rearrangeData {
    if(_arrayExpense != nil) {
        itemNames = [[NSMutableArray alloc] init];
        itemValues = [[NSMutableArray alloc] init];
        NSArray<ExpenseEvent *> *sortedEvent = [_arrayExpense sortedArrayUsingComparator:^NSComparisonResult(ExpenseEvent  *evt1, ExpenseEvent *evt2) {
            if(evt1.amount < evt2.amount)
                return NSOrderedDescending;
            return NSOrderedAscending;
        }];
        int count = 0;
        CGFloat other = 0;
        for (ExpenseEvent *evt in sortedEvent) {
            count++;
            if(count < 5) {
                [itemNames addObject:evt.desc];
                [itemValues addObject:[NSNumber numberWithInteger:evt.amount]];
            } else {
                other += evt.amount;
            }
        }
        [itemNames addObject:@"Other"];
        [itemValues addObject:[NSNumber numberWithFloat:other]];
    }
}

- (void)redesignLegend {
    self.legendOn = NO;
}

- (void)loadGraph {
    [self loadDataForPieChartItemValues:itemValues
                                 colors:itemColors
                           descriptions:itemNames];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    TimelineViewController *vc = [[TimelineViewController alloc] initWithNibName:@"TimelineViewController" bundle:[NSBundle mainBundle]];
    [vc setTitle:@"Expense Timeline"];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
