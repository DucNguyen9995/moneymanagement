//
//  ExpenseViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/28/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "ExpenseViewController.h"
#import "ExpenseAddViewController.h"
#import "BaseEventViewController.h"

@interface ExpenseViewController () {
    NSArray * cellLabels;
}

@end

@implementation ExpenseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Expense";
    cellLabels =@[@"Monthly Expense", @"Daily Expense", @"", @"Today Expense"];
    _tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_tblView setDataSource:self];
    [_tblView setDelegate:self];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [cellLabels count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellExpenseID];
    if(cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellExpenseID];
    [cell.textLabel setText:[cellLabels objectAtIndex:indexPath.row]];
    if([[cellLabels objectAtIndex:indexPath.row] isEqualToString:@""])
        cell.accessoryType = UITableViewCellAccessoryNone;
    else
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    BaseViewController *vc;
    switch (indexPath.row) {
        case 0:
            vc = [[BaseEventViewController alloc] initWithNibName:@"BaseEventViewController" bundle:[NSBundle mainBundle]];
            ((BaseEventViewController *)vc).events = [CommonUtils fetchTestArrayExpense:50];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        case 1:
            vc = [[BaseEventViewController alloc] initWithNibName:@"BaseEventViewController" bundle:[NSBundle mainBundle]];
            ((BaseEventViewController *)vc).events = [CommonUtils fetchTestArrayExpense:10];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        case 3:
            vc = [[ExpenseAddViewController alloc] initWithNibName:@"ExpenseAddViewController" bundle:[NSBundle mainBundle]];
            [self presentViewController:vc animated:YES completion:nil];
            break;
            
        default:
            break;
    }
    if(vc != nil)
        vc.title = [cellLabels objectAtIndex:indexPath.row];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
