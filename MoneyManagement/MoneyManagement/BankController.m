//
//  BankController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/15/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "BankController.h"

@implementation BankController

+ (instancetype)sharedInstance {
    static BankController *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[BankController alloc] init];
    });
    return instance;
}

- (BOOL)addBank:(Bank *)bank {
    if(!bank)
        return false;
    [APPDELEGATE.persistentContainer.viewContext insertObject:bank];
    if(![APPDELEGATE saveContext])
        return NO;
    return YES;
}

- (NSArray<Bank *> *)getAllBanks {
    NSError *error;
    NSArray<Bank *> *banks;
    // From date to date
    // All
    [APPDELEGATE saveContext];
    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:kEntityBankName];
    banks = [APPDELEGATE.persistentContainer.viewContext executeFetchRequest:req error:&error];
    if(error) {
        NSLog(@"%@", [error localizedDescription]);
        NSLog(@"%@", [error localizedFailureReason]);
        return nil;
    }
    return banks;
}

- (BOOL)deleteBank:(Bank *)bank {
    [APPDELEGATE.persistentContainer.viewContext deleteObject:bank];
    return YES;
}

@end
