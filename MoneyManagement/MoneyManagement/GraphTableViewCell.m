//
//  GraphTableViewCell.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/26/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "GraphTableViewCell.h"
#import <Masonry/Masonry.h>
#import <PNChart/PNChart.h>

#define kGraphBackgroundColor           PNGreen

@interface GraphTableViewCell () {
    
}
@property (nonatomic, assign) NSInteger numberOfPoints;
@end

@implementation GraphTableViewCell

static NSMutableArray<NSNumber *> *yA;
static NSMutableArray *xA;

- (instancetype)initWithYAxisData:(NSMutableArray<NSNumber *> *)yAxis
                        xAxisData:(NSMutableArray *)xAxis {
    self = [super init];
    if(yAxis == nil || [yAxis count] == 0) {
        return nil;
    }
    yA = yAxis;
    xA = xAxis;
    self.arrayOfXAxis = xAxis;
    self.arrayOfYAxis = yAxis;
    if(xAxis != nil && [xAxis count] != 0)
        self.numberOfPoints = [self.arrayOfXAxis count] >= [self.arrayOfYAxis count]?[self.arrayOfYAxis count]:[self.arrayOfXAxis count];
    else
        self.numberOfPoints = [self.arrayOfYAxis count];
    return self;
}

- (void)configureGraph {
#pragma mark - Graph Config
    // Create a gradient to apply to the bottom portion of the graph
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = {
        1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 0.0
    };
    
    // Apply the gradient to the bottom portion of the graph
    _graphView.gradientBottom = CGGradientCreateWithColorComponents(colorspace, components, locations, num_locations);
    _graphView.enableBezierCurve = YES;
    _graphView.animationGraphStyle = BEMLineAnimationDraw;
    _graphView.enableYAxisLabel = YES;
    _graphView.enableXAxisLabel = YES;
    _graphView.enableReferenceXAxisLines = YES;
    _graphView.enableReferenceYAxisLines = YES;
    _graphView.enableReferenceAxisFrame = YES;
    _graphView.lineDashPatternForReferenceYAxisLines = @[@(2),@(2)];
    _graphView.backgroundColor = kGraphBackgroundColor;
    _graphView.colorTop = kGraphBackgroundColor;
    _graphView.colorBottom = kGraphBackgroundColor;
    
#pragma mark - Average Line Config
    _graphView.averageLine.enableAverageLine = YES;
    _graphView.averageLine.alpha = 0.6;
    _graphView.averageLine.color = [UIColor darkGrayColor];
    _graphView.averageLine.width = 1;
}

#pragma mark - Graph DataSource
- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph {
    _numberOfPoints = [_arrayOfYAxis count];
    return _numberOfPoints;
}

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index {
    return [[self.arrayOfYAxis objectAtIndex:index] floatValue];
}

- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index {
    NSString *str = [self.arrayOfXAxis objectAtIndex:index];
    if(str == nil) str = @"";
    return str;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _graphView = [[BEMSimpleLineGraphView alloc] init];
    [self configureGraph];
    [_graphView setDataSource:self];
    [self addSubview:_graphView];
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    [_graphView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(insets);
    }];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
