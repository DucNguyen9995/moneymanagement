//
//  RatioGraphViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BaseChartViewController.h"

@interface RatioGraphViewController : BaseChartViewController

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, assign) CGRect fullFrame;
@property (nonatomic, assign) NSInteger spentMoney;
@property (nonatomic, assign) NSInteger allMoney;

@end
