//
//  IncomeViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/28/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BaseViewController.h"

#define kCellIncomeAddID               @"kCellIncomeMenu"

@interface IncomeViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end
