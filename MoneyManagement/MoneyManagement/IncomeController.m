//
//  IncomeController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/8/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "IncomeController.h"

@implementation IncomeController

+ (instancetype)sharedInstance {
    static IncomeController *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[IncomeController alloc] init];
    });
    return instance;
}

- (BOOL)addEvent:(IncomeEvent *)evt {
    if(!evt)
        return false;
    [APPDELEGATE.persistentContainer.viewContext insertObject:evt];
    if(![APPDELEGATE saveContext])
        return NO;
    [CommonUtils addIncomeEvent:evt];
    return YES;
}

- (NSArray<IncomeEvent *> *)getEventsFrom:(NSDate *)fromDate
                                       to:(NSDate *)toDate {
    NSError *error;
    NSArray<IncomeEvent *> *events;
    // From date to date
    if(fromDate != nil && toDate != nil) {
        NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:kEntityIncomeName];
        // From day1 to day2
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date >= %@ && date <= %@", fromDate, toDate];
        req.predicate = predicate;
//         Sort it if you want
//        NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
//        req.sortDescriptors = @[sortDesc];
        // Fetch req
        events = [APPDELEGATE.persistentContainer.viewContext executeFetchRequest:req error:&error];
    }
    // From start to date
    if(fromDate == nil && toDate != nil) {
        NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:kEntityIncomeName];
        // From day1 to day2
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date <= %@", toDate];
        req.predicate = predicate;
        
        events = [APPDELEGATE.persistentContainer.viewContext executeFetchRequest:req error:&error];
    }
    // From date to now
    if(fromDate != nil && toDate == nil) {
        NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:kEntityIncomeName];
        // From day1 to day2
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date >= %@", fromDate];
        req.predicate = predicate;
        
        events = [APPDELEGATE.persistentContainer.viewContext executeFetchRequest:req error:&error];
    }
    // All
    if(fromDate == nil && toDate == nil) {
        NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:kEntityIncomeName];
        events = [APPDELEGATE.persistentContainer.viewContext executeFetchRequest:req error:&error];
    }
    if(error) {
        NSLog(@"%@", [error localizedDescription]);
        NSLog(@"%@", [error localizedRecoverySuggestion]);
        return nil;
    }
    return events;
}

- (BOOL)deleteEvent:(IncomeEvent *)evt {
    [APPDELEGATE.persistentContainer.viewContext deleteObject:evt];
    return YES;
}

@end
