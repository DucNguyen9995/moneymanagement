//
//  ExpenseAddViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/5/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "ExpenseAddViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "BankController.h"
#import "ExpenseController.h"
#import "ExpenseEvent+CoreDataClass.h"
#import <ActionSheetPicker_3_0/ActionSheetPicker.h>

@interface ExpenseAddViewController () {
    NSInteger selectedCellIndex;
    ExpenseEvent *evt;
    NSDateFormatter *df;
}

@end

@implementation ExpenseAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadTest];
    [self customizeUIConfig];
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)loadTest {
    _arrCategory = [CommonUtils fetchCategories:20];
}

- (void)loadData {
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd, MMMM, yyyy"];
    selectedCellIndex = 0;
    self.title = @"Add Expense";
    [_tblViewTypes setDelegate:self];
    [_tblViewTypes setDataSource:self];
    evt = [[ExpenseEvent alloc] initWithContext:APPDELEGATE.persistentContainer.viewContext];
    [_btnDate setTitle:[df stringFromDate:[NSDate date]] forState:UIControlStateNormal & UIControlStateFocused];
}

- (void)customizeUIConfig {
    _btnCreate.layer.borderWidth = 1.0f;
    _btnCreate.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _btnCreate.layer.cornerRadius = 5.0f;
    _btnCashOrBank.layer.borderWidth = 1.0f;
    _btnCashOrBank.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_arrCategory == nil) {
        [self loadTest];
    }
    return [_arrCategory count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellExpenseCat];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellExpenseCat];
    }
    Category *cat = [_arrCategory objectAtIndex:indexPath.row];
    [cell.imageView setImage:[UIImage imageNamed:cat.iconname]];
    [cell.textLabel setText:cat.name];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == selectedCellIndex) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self resignAllFirstResponder];
    selectedCellIndex = indexPath.row;
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    for(UITableViewCell *unCheckCell in [tableView visibleCells]) {
        [unCheckCell setAccessoryType:UITableViewCellAccessoryNone];
    }
    evt.type = [_arrCategory objectAtIndex:indexPath.row];
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self resignAllFirstResponder];
}

- (void)resignAllFirstResponder {
    [_textFieldAmt resignFirstResponder];
    [_textFieldDesc resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnDidTap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnCashOrBankDidTap:(id)sender {
    NSArray *banks = [CommonUtils fetchBanks];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for(Bank *b in banks) {
        [arr addObject:b.name];
    }
    [ActionSheetStringPicker showPickerWithTitle:nil
                                            rows:arr
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           [_btnCashOrBank setTitle:selectedValue forState:UIControlStateNormal & UIControlStateHighlighted];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Action Picker Canceled");
                                     }
                                          origin:sender];
}
- (IBAction)btnDateDidTap:(id)sender {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:[NSDate date]];
    NSUInteger numberOfDaysInMonth = range.length;
    NSMutableArray *arrDay = [[NSMutableArray alloc] init];
    NSDateFormatter *dff = [[NSDateFormatter alloc] init];
    [dff setDateFormat:@"MMMM, yyyy"];
    for(int i = 1; i <= numberOfDaysInMonth; i++) {
        [arrDay addObject:[NSString stringWithFormat:@"%d, %@", i, [dff stringFromDate:[NSDate date]]]];
    }
    [ActionSheetStringPicker showPickerWithTitle:nil
                                            rows:arrDay
                                initialSelection:[calendar component:NSCalendarUnitDay fromDate:[NSDate date]] - 1
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSDateComponents *comps = [calendar components:NSUIntegerMax
                                                                                 fromDate:[NSDate date]];
                                           [comps setDay:selectedIndex + 1];
                                           [evt setDate:[calendar dateFromComponents:comps]];
                                           [_btnDate setTitle:[df stringFromDate:evt.date] forState:UIControlStateNormal & UIControlStateHighlighted];
                                       }
                                     cancelBlock:nil
                                          origin:sender];
}

#define kWarningNoDataAmount @"Please enter an amount of money"
#define kWarningNoDataDesc   @"Please enter reason for this event"

- (BOOL)dataValidated {
    if([_textFieldAmt.text floatValue] == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:kWarningNoDataAmount
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK"
                                                         style:UIAlertActionStyleDestructive
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           [_textFieldAmt becomeFirstResponder];
                                                       }];
        [alert addAction:action];
        [self presentViewController:alert
                           animated:YES
                         completion:nil];
        return NO;
    } else if([allTrim(_textFieldDesc.text) isEqualToString:@""]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:kWarningNoDataDesc
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK"
                                                         style:UIAlertActionStyleDestructive
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           [_textFieldDesc becomeFirstResponder];
                                                       }];
        [alert addAction:action];
        [self presentViewController:alert
                           animated:YES
                         completion:nil];
        return NO;
    }
    return YES;
}

- (IBAction)btnCreateDidTap:(id)sender {
    if(![self dataValidated])
        return;
    evt.amount = [_textFieldAmt.text intValue];
    evt.desc = _textFieldDesc.text;
    evt.active = YES;
    [[ExpenseController sharedInstance] addEvent:evt];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
