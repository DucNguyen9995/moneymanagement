//
//  TimelineViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/6/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "TimelineViewController.h"
#import "TimeLineViewControl.h"
#import <PNChart/PNColor.h>
#import <Masonry/Masonry.h>
#import "AppDelegate.h"

@interface TimelineViewController () {
    NSMutableArray<NSString *> *money;
    NSMutableArray<NSString *> *event;
    NSMutableArray<NSNumber *> *boldPosition;
    int status;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation TimelineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self test];
    [self displayTimeline];
    // Do any additional setup after loading the view from its nib.
}

- (void)test {
    _moneyEvents = [CommonUtils fetchTestArrayIncome:50];
}

- (bool)prepareDataForTimeline {
    if(![_moneyEvents count]) {
        [self backUpData];
        return false;
    }
    boldPosition = [[NSMutableArray alloc] init];
    _moneyEvents = [_moneyEvents sortedArrayUsingComparator:^NSComparisonResult(MoneyEvent *evt1, MoneyEvent *evt2) {
        NSComparisonResult res = [evt1.date compare:evt2.date];
        return res;
    }];
    money = [[NSMutableArray alloc] init];
    event = [[NSMutableArray alloc] init];
    NSDateFormatter *dFmt = [[NSDateFormatter alloc] init];
    [dFmt setDateFormat:@"dd-MM-yyyy"];
    NSMutableDictionary<NSString *, NSMutableArray<MoneyEvent *> *> *dictDay = [[NSMutableDictionary alloc] init];
    status = 0;
    NSDate *today = [NSDate date];
    NSMutableOrderedSet *oSet = [[NSMutableOrderedSet alloc] init];
    for(MoneyEvent *evt in _moneyEvents) {
        NSString *day = [dFmt stringFromDate:evt.date];
        NSMutableArray<MoneyEvent *> *temp = [dictDay objectForKey:day];
        if(!temp) {
            temp = [[NSMutableArray alloc] init];
        }
        [temp addObject:evt];
        [oSet addObject:day];
        [dictDay setObject:temp forKey:day];
    }
    int count = 0;
    for(NSString *key in oSet) {
        NSMutableArray<MoneyEvent *> *tmp = [dictDay objectForKey:key];
        CGFloat total = 0;
        NSMutableArray<NSString *> *moneyTmp = [[NSMutableArray alloc] init];
        [event addObject:key];
        [boldPosition addObject:[NSNumber numberWithInt:count++]];
        if([self date:today isAfterOrEqualDate:[dFmt dateFromString:key]])
            status++;
        for(MoneyEvent *evt in tmp) {
            total += evt.amount;
            [moneyTmp addObject:[NSString stringWithFormat:@"$%d", evt.amount]];
            [event addObject:evt.desc];
            if([self date:today isAfterOrEqualDate:evt.date])
                status++;
            count++;
        }
        [money addObject:[NSString stringWithFormat:@"$%.0f", total]];
        for(NSString *str in moneyTmp) {
            [money addObject:str];
        }
    }
    return true;
}

- (BOOL)date:(NSDate *)date_one isAfterOrEqualDate:(NSDate *)date_two {
    NSCalendar *cal = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitDay | NSCalendarUnitMonth;
    NSDateComponents *com1 = [cal components:unitFlags fromDate:date_one];
    NSDateComponents *com2 = [cal components:unitFlags fromDate:date_two];
    if([com1 month] > [com2 month])
        return true;
    if([com1 month] < [com2 month])
        return false;
    return [com1 day] >= [com2 day];
}

- (void)displayTimeline {
    if(![self prepareDataForTimeline]) {
        return;
    }
    TimeLineViewControl *tlView = [TimeLineViewControl alloc];
    tlView.completedLineColor = PNRed;
    tlView.strokeColor = PNGreen;
    tlView.boldPositions = boldPosition;
    tlView.highlightedColor = [UIColor redColor];
    CGRect rect = [[UIScreen mainScreen] bounds];
    tlView = [tlView initWithTimeArray:money
               andTimeDescriptionArray:event
                      andCurrentStatus:status + 1
                              andFrame:rect];
    [self.scrollView addSubview:tlView];
    [_scrollView setContentSize:CGSizeMake(rect.size.width, tlView.viewheight)];
    _scrollView.contentInset = UIEdgeInsetsMake(0, 0, self.tabBarController.tabBar.frame.size.height, 0);
}

- (void)backUpData {
    UILabel *noData = [[UILabel alloc] init];
    [noData setText:@"No data to display"];
    [noData setTextAlignment:NSTextAlignmentCenter];
    [noData setTextColor:[UIColor lightGrayColor]];
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.view addSubview:noData];
    [self.scrollView setHidden:YES];
    [noData mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(insets);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
