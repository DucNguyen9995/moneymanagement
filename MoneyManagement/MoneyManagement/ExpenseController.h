//
//  ExpenseController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/8/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "MoneyController.h"
#import "ExpenseEvent+CoreDataClass.h"

#define kEntityExpenseName                              @"ExpenseEvent"

@interface ExpenseController : MoneyController

+ (instancetype)sharedInstance;

- (BOOL)addEvent:(ExpenseEvent *)evt;
- (NSArray<ExpenseEvent *> *)getEventsFrom:(NSDate *)fromDate
                                       to:(NSDate *)toDate;
- (BOOL)deleteEvent:(ExpenseEvent *)evt;

@end
