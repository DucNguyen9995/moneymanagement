//
//  BaseChartViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/20/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BaseChartViewController.h"

@implementation CustomPieChart : PNPieChart

- (void)recompute {
    [super recompute];
    if(!(_ringSize == 0)) {
        if(_ringSize < 60)
            _ringSize = 60;
        if(_ringSize > self.outerCircleRadius)
            _ringSize = self.outerCircleRadius;
        self.innerCircleRadius = self.outerCircleRadius - _ringSize;
    }
}

@end

@interface BaseChartViewController () {
    UIView *legend;
    NSArray *items;
}

@end

@implementation BaseChartViewController
@synthesize pieChart;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    pieChart.duration = 1;
    pieChart.displayAnimated = YES;
    [pieChart strokeChart];
}

- (BaseChartViewController *)initWithChartValues:(NSMutableArray<NSNumber *> *)values
                                     colors:(NSMutableArray<UIColor  *> *)colors
                                    descriptions:(NSMutableArray<NSString *> *)descs {
    self = [super init];
    [self configurePieChart];
    [self loadDataForPieChartItemValues:values colors:colors descriptions:descs];
    return self;
}

- (void)configurePieChart {
    CGRect rect = CGRectMake(20.0, (_identifier == 1?self.navigationController.navigationBar.frame.size.height:0) + 20, [[UIScreen mainScreen] bounds].size.width - 40, [[UIScreen mainScreen] bounds].size.width - 40);
    pieChart = [[CustomPieChart alloc] initWithFrame:rect];
    pieChart.showOnlyValues = self.showOnlyValue;
    pieChart.descriptionTextColor = [UIColor whiteColor];
    pieChart.descriptionTextFont  = [UIFont fontWithName:@"Avenir-Medium" size:14.0];
    [pieChart strokeChart];
    [self.view addSubview:pieChart];
    pieChart.legendStyle = _legendStyle;
    pieChart.shouldHighlightSectorOnTouch = NO;
}

- (bool)loadDataForPieChartItemValues:(NSMutableArray<NSNumber *> *)values
                               colors:(NSMutableArray<UIColor  *> *)colors
                         descriptions:(NSMutableArray<NSString *> *)descs {
    [self configurePieChart];
    NSUInteger count;
    if(descs != nil)
        count = values.count < colors.count? values.count < descs.count ? values.count:descs.count:colors.count < descs.count ? colors.count:descs.count;
    else
    count = values.count < colors.count?values.count:colors.count;
    if(count > values.count)
        return NO;
    NSMutableArray<PNPieChartDataItem *> *data = [[NSMutableArray alloc] init];
    for(int i = 0; i < values.count; i++) {
        PNPieChartDataItem *item;
        if(descs == nil) {
            item = [PNPieChartDataItem dataItemWithValue:[[values objectAtIndex:i] floatValue]
                                                   color:[colors objectAtIndex:i]];
        } else {
        item = [PNPieChartDataItem dataItemWithValue:[[values objectAtIndex:i] floatValue]
                                        color:[colors objectAtIndex:i]
                                  description:[[descs objectAtIndex:i] stringByAppendingString:@"  "]];
        }
        [data addObject:item];
    }
    items = [NSArray arrayWithArray:data];
    [pieChart updateChartData:data];
    pieChart.ringSize = _ringSize;
    legend = [pieChart getLegendWithMaxWidth:200];
    
    //Move legend to the desired position and add to view
    [legend setFrame:CGRectMake((self.view.frame.size.width - legend.frame.size.width) / 2 + 5, pieChart.frame.origin.y + pieChart.frame.size.height + 20, legend.frame.size.width, legend.frame.size.height)];
    [self reloadDataWithLegend:descs != nil];
    return YES;
}

- (void)reloadDataWithLegend:(BOOL)option {
    pieChart.duration = 1;
    pieChart.displayAnimated = YES;
    [pieChart strokeChart];
    [legend removeFromSuperview];
    if(option && _legendOn) {
        [self.view addSubview:legend];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
