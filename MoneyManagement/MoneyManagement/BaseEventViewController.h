//
//  BaseEventViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/13/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "BaseViewController.h"
#import "MTEventTableViewCell.h"
#import "MoneyEvent+CoreDataClass.h"

@interface BaseEventViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource, MTEventTableViewCellDelegate>

@property (nonatomic, strong) NSArray<MoneyEvent *> *events;

@end
