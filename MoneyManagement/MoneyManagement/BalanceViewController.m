//
//  BalanceViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/26/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BalanceViewController.h"
#import "ValueTableViewCell.h"
#import "GraphTableViewCell.h"

#define kCellValueId                @"ValueCell"
#define kCellTestId                 @"TestCell"


@interface BalanceViewController () {
    NSMutableArray<NSNumber *> *bankMoney;
    NSMutableArray<NSString *> *bankNames;
}

@end

@implementation BalanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Balance Statistic";
    [self prepareData];
    // Do any additional setup after loading the view from its nib.
}

- (void)prepareData {
    NSArray<Bank *> *banks = [CommonUtils fetchBanks];
    Bank *cash = [banks objectAtIndex:0];
    _currentCash = cash.amount;
    banks = [banks subarrayWithRange:NSMakeRange(1, [banks count] - 1)];
    _arrayCashOverTime = [[NSMutableArray alloc] init];
    for(BankDiary *dia in cash.diaries) {
        [_arrayCashOverTime addObject:[NSNumber numberWithFloat:dia.amount]];
    }
    bankNames = [[NSMutableArray alloc] init];
    bankMoney = [[NSMutableArray alloc] init];
    for (id key in _dictBanks) {
        [bankNames addObject:key];
        [bankMoney addObject:[_dictBanks objectForKey:key]];
    }
    numOfCells = 3 + [_dictBanks count];
    [_tableOverViewBalance setDelegate:self];
    [_tableOverViewBalance setDataSource:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

static NSInteger numOfCells;

- (NSInteger)totalBanks {
    NSInteger total = 0;
    for(NSNumber *num in bankMoney) {
        total += [num integerValue];
    }
    return total;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return numOfCells;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    switch (indexPath.row) {
        case 0:
            cell = [self prepareDescCell:@"Cash"
                                   value:[NSString stringWithFormat:@"%.0f", _currentCash]
                               tableView:tableView
                               indexPath:indexPath];
            break;
        case 1:
            cell = [self prepareGraphCellWithX:nil y:_arrayCashOverTime tableView:tableView indexPath:indexPath];
            if(cell == nil)
                cell = [self prepareCellHolder:tableView indexPath:indexPath];
            break;
        case 2:
            cell = [self prepareDescCell:@"Bank" value:[NSString stringWithFormat:@"%ld", [self totalBanks]] tableView:tableView indexPath:indexPath];
            break;
        default:
            cell = [self prepareDescCell:[@" + " stringByAppendingString:[bankNames objectAtIndex:indexPath.row - 3]]
                                   value:[NSString stringWithFormat:@"%@", [bankMoney objectAtIndex:indexPath.row - 3]]
                               tableView:tableView
                               indexPath:indexPath];
            break;
    }
    return cell;
}

- (UITableViewCell *)prepareCellHolder:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellTestId];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellTestId
                                                            forIndexPath:indexPath];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellTestId];
    }
    return cell;
    
}

- (UITableViewCell *)prepareDescCell:(NSString *)desc
                               value:(NSString *)value
                           tableView:(UITableView *)tableView
                           indexPath:(NSIndexPath *)indexPath {
    [tableView registerClass:[ValueTableViewCell class] forCellReuseIdentifier:kCellValueId];
    [tableView registerNib:[UINib nibWithNibName:@"ValueTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCellValueId];
    ValueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellValueId forIndexPath:indexPath];
    if(cell == nil) {
        cell = [[ValueTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: kCellValueId];
    }
    [cell.lblDesc setText:desc];
    if(![value containsString:@"$"]) {
        [cell.lblValue setText:[value stringByAppendingString:@"$"]];
    } else {
        [cell.lblValue setText:value];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 1)
        return 200;
    return 48;
}

- (UITableViewCell *)prepareGraphCellWithX:(NSMutableArray *)xAxis
                                         y:(NSMutableArray<NSNumber *> *)yAxis
                                 tableView:(UITableView *)tableView
                                 indexPath:(NSIndexPath *)indexPath {
    GraphTableViewCell *cell = nil;
    cell = [[GraphTableViewCell alloc] initWithYAxisData:yAxis xAxisData:xAxis];
    [cell awakeFromNib];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
