//
//  ExpenseAddViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/5/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "BaseViewController.h"

#define kCellExpenseCat             @"kCellExpenseCat"

@interface ExpenseAddViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textFieldAmt;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDesc;
@property (weak, nonatomic) IBOutlet UITableView *tblViewTypes;
@property (weak, nonatomic) IBOutlet UIButton *btnCreate;
@property (weak, nonatomic) IBOutlet UIButton *btnCashOrBank;
@property (weak, nonatomic) IBOutlet UIButton *btnDate;
@property (nonatomic, strong) NSArray<Category *> *arrCategory;
@end
