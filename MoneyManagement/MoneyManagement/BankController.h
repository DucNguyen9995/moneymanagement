//
//  BankController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/15/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "MoneyController.h"
#import "Bank+CoreDataClass.h"

#define kAttrBankNameCash                           @"Cash"
#define kAttrBankNameDefault1                       @"Vietinbank"
#define kAttrBankNameDefault2                       @"TPBank"
#define kAttrBankAmountDefault                      0
#define kEntityBankName                             @"Bank"
#define kEntityBankDiaryName                        @"BankDiary"

@interface BankController : NSObject

+ (instancetype)sharedInstance;

- (BOOL)addBank:(Bank *)bank;
- (NSArray<Bank *> *)getAllBanks;
- (BOOL)deleteBank:(Bank *)bank;

@end
