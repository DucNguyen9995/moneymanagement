//
//  SettingsViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/23/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController () {
    NSArray<NSString *> *cellImageNames;
    NSArray<NSString *> *cellLabelText;
}


@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    cellLabelText = @[@"Passcode lock", @"Currency", @"Language", @"", @"Help", @"Share", @"Rate", @"Feedback", @"FB Page"];
    cellImageNames = @[];
    _tblSettings.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_tblSettings setDataSource:self];
    [_tblSettings setDelegate:self];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [cellLabelText count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellSettingsID];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellSettingsID];
    }
    if([cellLabelText count] < indexPath.row + 1)
        return cell;
    [cell.textLabel setText:[cellLabelText objectAtIndex:indexPath.row]];
    if([cellImageNames count] < indexPath.row + 1)
        return cell;
    if(![[cellLabelText objectAtIndex:indexPath.row] isEqualToString:@""])
        [cell.imageView setImage:[UIImage imageNamed:[cellImageNames objectAtIndex:indexPath.row]]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            [self setPasscode];
            break;
        case 5:
            [self share:@[kAppleAppLink]];
            break;
        case 6:
            [self rateApp];
            break;
        case 8:
            [self openFacebook];
            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)rateApp {
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:kAppleAppLink]])
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kAppleAppLink]
                                           options:@{}
                                 completionHandler:nil];
}

- (void)share:(NSArray *)sharedObjects {
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:sharedObjects
                                                                                         applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (void)openFacebook {
    NSURL *facebookURL = [NSURL URLWithString:kFacebookAppLink];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
        [[UIApplication sharedApplication] openURL:facebookURL
                                           options:@{}
                                 completionHandler:nil];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kFacebookLink]
                                           options:@{}
                                 completionHandler:nil];
    }
}

- (IBAction)backButtonDidTap:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (void)setPasscode {
    if([[VENTouchLock sharedInstance] isPasscodeSet]) {
        VENTouchLockEnterPasscodeViewController *enterVC = [[VENTouchLockEnterPasscodeViewController alloc] init];
        [self presentViewController:[enterVC embeddedInNavigationController] animated:YES completion:nil];
        [[VENTouchLock sharedInstance] deletePasscode];
    }
    VENTouchLockCreatePasscodeViewController *createVC = [[VENTouchLockCreatePasscodeViewController alloc] init];
    [self presentViewController:[createVC embeddedInNavigationController] animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
