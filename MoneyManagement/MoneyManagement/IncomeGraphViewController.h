//
//  IncomeGraphViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BaseChartViewController.h"

@interface IncomeGraphViewController : BaseChartViewController

@property (nonatomic, strong) NSMutableArray<NSNumber *> *expectedMoney;
@property (nonatomic, strong) NSMutableArray<NSNumber *> *unexpectedMoney;

@end
