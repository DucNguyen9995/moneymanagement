//
//  BaseChartViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/20/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <PNChart/PNChart.h>

@interface CustomPieChart : PNPieChart

@property(nonatomic, assign) CGFloat ringSize;

@end

@interface BaseChartViewController : BaseViewController
@property (nonatomic, assign) PNLegendItemStyle legendStyle;
@property (nonatomic, assign) CGFloat ringSize;
@property (nonatomic, assign) NSUInteger identifier;
@property (nonatomic, getter=legendOn) BOOL legendOn;
@property (nonatomic, strong) UIViewController *segueController;
@property (nonatomic, strong) CustomPieChart *pieChart;
@property (nonatomic, assign) BOOL showOnlyValue;
- (BaseChartViewController *)initWithChartValues:(NSMutableArray<NSNumber *> *)values
                                          colors:(NSMutableArray<UIColor  *> *)colors
                                    descriptions:(NSMutableArray<NSString *> *)descs;

- (bool)loadDataForPieChartItemValues:(NSMutableArray<NSNumber *> *)values
                               colors:(NSMutableArray<UIColor  *> *)colors
                         descriptions:(NSMutableArray<NSString *> *)descs;


@end
