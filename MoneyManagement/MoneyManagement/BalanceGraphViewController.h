//
//  BalanceGraphViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BaseChartViewController.h"

@interface BalanceGraphViewController : BaseChartViewController

@property (nonatomic, assign) NSInteger cash;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSNumber *> *dictBank;

@end
