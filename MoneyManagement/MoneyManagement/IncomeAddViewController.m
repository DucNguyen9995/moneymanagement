//
//  IncomeAddViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/5/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "IncomeAddViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <ActionSheetPicker_3_0/ActionSheetPicker.h>
#import "BankController.h"
#import "IncomeController.h"

@interface IncomeAddViewController () {
    IncomeEvent *evt;
    NSDateFormatter *df;
}
@property (nonatomic, strong) NSArray<Bank *> *banks;
@end

@implementation IncomeAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareData];
    [self customizeViewConfig];
    // Do any additional setup after loading the view from its nib.
}

- (void)prepareData {
    if(!evt)
        evt = [[IncomeEvent alloc] initWithContext:APPDELEGATE.persistentContainer.viewContext];
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd, MMMM, yyyy"];
    _banks = [CommonUtils fetchBanks];
    [_btnOptionalBankCash setTitle:@"Cash" forState:UIControlStateNormal & UIControlStateHighlighted];
    [evt setSource:[_banks objectAtIndex:0]];
    [_btnDate setTitle:[df stringFromDate:[NSDate date]] forState:UIControlStateNormal & UIControlStateFocused];
    [evt setDate:[NSDate date]];
}

- (void)customizeViewConfig {
    self.title = @"New Income";
    [_checkOneTime setDelegate:self];
    [_checkOneTime setOn:YES animated:YES];
    [_checkMonthly setOn:NO animated:NO];
    [evt setActive:NO];
    [self hideDate:YES];
    [_checkMonthly setDelegate:self];
    _btnCreate.layer.borderWidth = 1.0f;
    _btnCreate.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _btnCreate.layer.cornerRadius = 5.0f;
    _btnOptionalBankCash.layer.borderWidth = 1.0f;
    _btnOptionalBankCash.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _btnDate.layer.borderWidth = 1.0f;
    _btnDate.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [_btnOptionalBankCash setTitle:@"Cash" forState:UIControlStateNormal & UIControlStateFocused];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [_textFieldMoneyAmt resignFirstResponder];
    [_textFieldDesc resignFirstResponder];
}
- (IBAction)btnBankOrCashDidTap:(id)sender {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for(Bank *bank in _banks) {
        [arr addObject:[bank name]];
    }
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:arr
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           [evt setSource:[_banks objectAtIndex:selectedIndex]];
                                       }
                                     cancelBlock:nil
                                          origin:sender];
}

- (IBAction)btnBackDidTap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnDateDidTap:(id)sender {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:[NSDate date]];
    NSUInteger numberOfDaysInMonth = range.length;
    NSMutableArray *arrDay = [[NSMutableArray alloc] init];
    NSDateFormatter *dff = [[NSDateFormatter alloc] init];
    [dff setDateFormat:@"MMMM, yyyy"];
    for(int i = 1; i <= numberOfDaysInMonth; i++) {
        [arrDay addObject:[NSString stringWithFormat:@"%d, %@", i, [dff stringFromDate:[NSDate date]]]];
    }
    [ActionSheetStringPicker showPickerWithTitle:nil
                                            rows:arrDay
                                initialSelection:[calendar component:NSCalendarUnitDay fromDate:[NSDate date]] - 1
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSDateComponents *comps = [calendar components:NSUIntegerMax
                                                                                 fromDate:[NSDate date]];
                                           [comps setDay:selectedIndex + 1];
                                           [evt setDate:[calendar dateFromComponents:comps]];
                                           [_btnDate setTitle:[df stringFromDate:evt.date] forState:UIControlStateNormal & UIControlStateHighlighted];
                                       }
                                     cancelBlock:nil
                                          origin:sender];
}

- (void)hideDate:(BOOL)hidden {
    [_lblDate setHidden:hidden];
    [_btnDate setHidden:hidden];
}

- (void)didTapCheckBox:(BEMCheckBox *)checkBox {
    if(checkBox == _checkOneTime) {
        [_checkOneTime setOn:YES animated:YES];
        [_checkMonthly setOn:NO animated:YES];
        [evt setActive:NO];
        [self hideDate:YES];
    } else if (checkBox == _checkMonthly) {
        [_checkMonthly setOn:YES animated:YES];
        [_checkOneTime setOn:NO animated:YES];
        [evt setActive:YES];
        [self hideDate:NO];
    }
}

#define kWarningNoDataAmount @"Please enter an amount of money"
#define kWarningNoDataDesc   @"Please enter reason for this event"

- (BOOL)dataValidated {
    if([_textFieldMoneyAmt.text floatValue] == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:kWarningNoDataAmount
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK"
                                                         style:UIAlertActionStyleDestructive
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           [_textFieldMoneyAmt becomeFirstResponder];
                                                       }];
        [alert addAction:action];
        [self presentViewController:alert
                           animated:YES
                         completion:nil];
        return NO;
    } else if([allTrim(_textFieldDesc.text) isEqualToString:@""]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:kWarningNoDataDesc
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK"
                                                         style:UIAlertActionStyleDestructive
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           [_textFieldDesc becomeFirstResponder];
                                                       }];
        [alert addAction:action];
        [self presentViewController:alert
                           animated:YES
                         completion:nil];
        return NO;
    }
    return YES;
}

- (IBAction)btnCreateDidTap:(id)sender {
    if(![self dataValidated])
        return;
    [evt setAmount:[_textFieldMoneyAmt.text floatValue]];
    [evt setDesc:_textFieldDesc.text];
    [[IncomeController sharedInstance] addEvent:evt];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
