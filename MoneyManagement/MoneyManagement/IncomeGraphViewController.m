//
//  IncomeGraphViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "IncomeGraphViewController.h"
#import "TimelineViewController.h"

@interface IncomeGraphViewController () {
    CGFloat expectedTotal;
    CGFloat unexpectedTotal;
}

@end

@implementation IncomeGraphViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.legendOn = YES;
    self.ringSize = 5000;
    self.legendStyle = PNLegendItemStyleStacked;
    [self loadGraph];
    // Do any additional setup after loading the view.
}

- (void)caculate {
    for (NSNumber *mon in _expectedMoney) {
        expectedTotal += [mon floatValue];
    }
    for (NSNumber *mon in _unexpectedMoney) {
        unexpectedTotal += [mon floatValue];
    }
}

- (void)loadGraph {
    [self caculate];
    NSMutableArray<NSNumber *> *itemValues = [[NSMutableArray alloc] init];
    [itemValues addObject:[NSNumber numberWithFloat:expectedTotal]];
    [itemValues addObject:[NSNumber numberWithFloat:unexpectedTotal]];
    NSMutableArray<UIColor *> *itemColors = [[NSMutableArray alloc] init];
    [itemColors addObject:PNLightGreen];
    [itemColors addObject:PNRed];
    [self loadDataForPieChartItemValues:itemValues
                                 colors:itemColors
                           descriptions:[NSMutableArray arrayWithObjects:@"Expected", @"Unexpected", nil]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    TimelineViewController *vc = [[TimelineViewController alloc] initWithNibName:@"TimelineViewController" bundle:[NSBundle mainBundle]];
    [vc setTitle:@"Income Timeline"];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
