//
//  ExpenseViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/28/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BaseViewController.h"

#define kCellExpenseID              @"kCellExpense"

@interface ExpenseViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end
