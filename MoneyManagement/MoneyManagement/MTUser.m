//
//  MTUser.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/16/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "MTUser.h"

@implementation MTUser

+ (instancetype)sharedInstance {
    static MTUser *user;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        user = [[MTUser alloc] init];
        
    });
    return user;
}

- (NSInteger)spentMoneyOfThisMonth {
#warning Do logic to calculate expense of this month and save to UserInfo
    [[NSUserDefaults standardUserDefaults] setInteger:100
                                               forKey:@"spent"];
    return [[[NSUserDefaults standardUserDefaults] valueForKey:@"spent"] integerValue];
}

@end
