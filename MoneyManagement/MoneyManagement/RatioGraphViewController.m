//
//  RatioGraphViewController.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "RatioGraphViewController.h"
#import <Masonry/Masonry.h>

#define kLabelBannerPercent                 @"You have spent %.0f%% of your income"
#define kLabelNoData                        @"You don't have any data to display"

@interface RatioGraphViewController ()

@end

@implementation RatioGraphViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(_allMoney == 0)
       [self showNoData];
    else {
        self.ringSize = 10;
        self.legendOn = NO;
        [self loadData];
        [self displayDescriptionWithPercent:_spentMoney/1.0/_allMoney * 100];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)loadData {
    [self hideNoData];
    NSMutableArray<NSNumber *> *itemValues = [[NSMutableArray alloc] init];
    [itemValues addObject:[NSNumber numberWithFloat:_spentMoney]];
    [itemValues addObject:[NSNumber numberWithFloat:_allMoney - _spentMoney]];
    NSMutableArray<UIColor *> *itemColors = [[NSMutableArray alloc] init];
    [itemColors addObject:PNLightGreen];
    [itemColors addObject:PNLightBlue];
    [self loadDataForPieChartItemValues:itemValues
                                 colors:itemColors
                           descriptions:nil];
}

- (void)hideNoData {
    [self.containerView setHidden:YES];
}

- (void)showNoData {
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    UILabel *noDataLbl = [[UILabel alloc] init];
    [noDataLbl setText:kLabelNoData];
    [noDataLbl setBackgroundColor:[UIColor whiteColor]];
    [noDataLbl setTextColor:[UIColor lightGrayColor]];
    [noDataLbl setTextAlignment:NSTextAlignmentCenter];
    [self.containerView addSubview:noDataLbl];
    [noDataLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.containerView).with.insets(insets);
    }];
}

- (void)displayDescriptionWithPercent:(CGFloat)percent {
    UILabel *lblDesc = [[UILabel alloc] init];
    [lblDesc setTextAlignment:NSTextAlignmentCenter];
    [lblDesc setText:[NSString stringWithFormat:kLabelBannerPercent, percent]];
    [self.view addSubview:lblDesc];
    [lblDesc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pieChart.mas_bottom).with.offset(0);
        make.bottom.equalTo(self.view.mas_bottom).with.offset(0);
        make.right.equalTo(self.pieChart.mas_right).with.offset(0);
        make.left.equalTo(self.pieChart.mas_left).with.offset(0);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
