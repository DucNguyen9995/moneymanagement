//
//  MTEventTableViewCell.m
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/13/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "MTEventTableViewCell.h"

@implementation MTEventTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _checkBox.delegate = self;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)didTapCheckBox:(BEMCheckBox *)checkBox {
    if(_delegate != nil)
        [_delegate didTapCheckBox:checkBox];
    else
        NSLog(@"Do somethings about the checkBox you jerk!\nBy using delegate");
}

- (IBAction)btnDeleteDidTap:(id)sender {
    if(_delegate != nil)
        [_delegate executeDeleteCellWithBtn:(UIButton *)sender];
    else
        NSLog(@"Do somethings about the delete you jerk!\nBy using delegate");
}
@end
