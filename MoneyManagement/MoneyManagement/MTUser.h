//
//  MTUser.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/16/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTUser : NSObject

+ (instancetype)sharedInstance;
- (NSInteger)spentMoneyOfThisMonth;

@end
