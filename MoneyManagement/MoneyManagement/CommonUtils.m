//
//  CommonUtils.m
//  WebSourceViewerAndPlayer
//
//  Created by Duc Nguyen on 11/21/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "CommonUtils.h"

@implementation CommonUtils

+ (UIImage*)resizeImage:(UIImage*)image toFitWidth:(CGFloat)width
                            height:(CGFloat)height {
    UIImage* resizedImage;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, height), NO, 0.0f);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}

+ (void)fatalInClass:(NSString *)className
            atMethod:(NSString *)methodName
           whileTask:(NSString *)taskName {
    NSLog(@"[FATAL] Error happens in class %@ at method %@() while %@",className, methodName, taskName);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:@"Sorry but a fatal error have just been found.\nSorry for this inconvinience."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       [alert dismissViewControllerAnimated:YES
                                                                                 completion:^{
                                                                                     exit(EXIT_FAILURE);
                                                                                 }];
                                                   }];
    [alert addAction:cancelAction];
    [[CommonUtils topMostViewController] presentViewController:alert
                                                      animated:YES
                                                    completion:nil];
}

+ (UIViewController *)topMostViewController {
    return [self topMostViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

+ (UIViewController *)topMostViewControllerWithRootViewController:(UIViewController *)rootViewController {
    if([rootViewController isKindOfClass:[UITabBarController class]]) {
        return [self topMostViewControllerWithRootViewController:((UITabBarController *)rootViewController).selectedViewController];
    } else if([rootViewController isKindOfClass:[UINavigationController class]]) {
        return [self topMostViewControllerWithRootViewController:((UINavigationController *)rootViewController).visibleViewController];
    } else if(rootViewController.presentedViewController) {
        return [self topMostViewControllerWithRootViewController:rootViewController];
    } else
        return rootViewController;
}


#pragma mark - This is just for Money Tracker
/*
 This is just for Money Tracker but it's an example for all other
 Create an sample of the data to test out
 */


static NSArray<ExpenseEvent *> * expenseEvents;
static NSArray<IncomeEvent *> * incomeEvents;
static NSArray<Bank *> * banks;
static NSArray<Category *> *arrCate;

+ (void)addExpenseEvent:(ExpenseEvent *)evt {
    expenseEvents = [expenseEvents arrayByAddingObject:evt];
    expenseEvents = [expenseEvents subarrayWithRange:NSMakeRange(1, [expenseEvents count] - 1)];
}

+ (void)addIncomeEvent:(IncomeEvent *)evt {
    incomeEvents = [incomeEvents arrayByAddingObject:evt];
    incomeEvents = [incomeEvents subarrayWithRange:NSMakeRange(1, [incomeEvents count] - 1)];
}

+ (NSArray<ExpenseEvent *> *)fetchTestArrayExpense:(NSUInteger)amount {
    if(!expenseEvents) {
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        [fmt setDateFormat:@"dd/MM/yyyy"];
        NSArray *arr = @[@"Borrow return", @"Mortage", @"Wine", @"iPhone", @"Girlfriend"];
        NSArray *arrAmt = @[@100, @200, @300, @400, @500, @600, @700, @630, @720];
        NSMutableArray<MoneyEvent *> *temp = [[NSMutableArray alloc] init];
        NSArray *bArr = [CommonUtils fetchBanks];
        NSArray *cArr = [CommonUtils fetchCategories:50];
        for(int i = 0; i < amount; i++) {
            NSString *str = [NSString stringWithFormat:@"%2d/%2d/%4d", arc4random()%5 + 1, arc4random()%2 + 1, 2016];
            ExpenseEvent *evt = [[ExpenseEvent alloc] initWithContext:APPDELEGATE.persistentContainer.viewContext];
            evt.date = [fmt dateFromString:str];
            evt.amount = [[arrAmt objectAtIndex:arc4random() % [arrAmt count] ] floatValue];
            evt.type = [cArr objectAtIndex:arc4random() % [cArr count]];
            evt.desc = [arr objectAtIndex:arc4random() % [arr count]];
            evt.source = [bArr objectAtIndex:arc4random() % [arr count]];
            evt.active = arc4random() % 2 ?YES:NO;
            [temp addObject:evt];
        }
        expenseEvents = [NSArray arrayWithArray:temp];
    }
    if(amount < 50) {
        return [expenseEvents subarrayWithRange:NSMakeRange(0, amount > 50?50:amount)];
    }
    return expenseEvents;
}

+ (NSArray<IncomeEvent *> *)fetchTestArrayIncome:(NSUInteger)amount {
    if(!incomeEvents) {
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        [fmt setDateFormat:@"dd/MM/yyyy"];
        NSArray *arr = @[@"Vegetables", @"Fruits", @"Insurance", @"Boss", @"Wife"];
        NSArray *arrAmt = @[@100, @200, @300, @400, @500, @600, @700, @630, @720];
        NSMutableArray<MoneyEvent *> *temp = [[NSMutableArray alloc] init];
        NSArray *bArr = [CommonUtils fetchBanks];
        for(int i = 0; i < 50; i++) {
            NSString *str = [NSString stringWithFormat:@"%2d/%2d/%4d", arc4random()%5 + 1, arc4random()%2 + 1, 2016];
            IncomeEvent *evt = [[IncomeEvent alloc] initWithContext:APPDELEGATE.persistentContainer.viewContext];
            evt.date = [fmt dateFromString:str];
            evt.amount = [[arrAmt objectAtIndex:arc4random() % [arrAmt count] ] floatValue];
            evt.source = [bArr objectAtIndex:arc4random() % [arr count]];
            evt.desc = [arr objectAtIndex:arc4random() % [arr count]];
            evt.active = arc4random() % 2 ?YES:NO;
            [temp addObject:evt];
        }
        incomeEvents = [NSArray arrayWithArray:temp];
    }
    if(amount < 50) {
        return [incomeEvents subarrayWithRange:NSMakeRange(0, amount > 50?50:amount)];
    }
    return incomeEvents;
}

+ (NSArray<Bank *> *)fetchBanks {
    if(!banks) {
        NSArray *nArray = @[@"Cash", @"Vietinbank", @"TPBank", @"VPBank", @"BIDV", @"Agribank"];
        NSArray *aArray = @[@645, @123, @445, @823, @2347, @236];
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        for (int i = 0; i < 6; i++) {
            Bank *bank = [[Bank alloc] initWithContext:APPDELEGATE.persistentContainer.viewContext];
            bank.name = [nArray objectAtIndex:i];
            bank.amount = [[aArray objectAtIndex:i] floatValue];
            for(BankDiary *diary in [CommonUtils fetchBankDiaries:10]) {
                [bank addDiariesObject:diary];
            }
            [temp addObject:bank];
        }
        banks = [NSArray arrayWithArray:temp];
    }
    return banks;
}

+ (NSArray<BankDiary *> *)fetchBankDiaries:(NSUInteger)amount {
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"dd/MM/yyyy"];
    NSArray *arrAmt = @[@100, @200, @300, @400, @500, @600, @700, @630, @720];
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    for(int i = 0; i < amount; i++) {
        NSString *str = [NSString stringWithFormat:@"%2d/%2d/%4d", arc4random()%5 + 1, arc4random()%2 + 1, 2016];
        BankDiary *diary = [[BankDiary alloc] initWithContext:APPDELEGATE.persistentContainer.viewContext];
        diary.date = [fmt dateFromString:str];
        for(BankDiary *dia in temp)
            if([diary.date isEqualToDate:dia.date])
                continue;
        diary.amount = [[arrAmt objectAtIndex:arc4random() % [arrAmt count]] floatValue];
        [temp addObject:diary];
    }
    return [NSArray arrayWithArray:temp];
}

+ (NSArray<Category *> *)fetchCategories:(NSUInteger)amount {
    if(!arrCate) {
        NSMutableArray<Category *> *temp = [[NSMutableArray alloc] init];
        for(int i = 0; i < 50; i++) {
            NSString *str = [NSString stringWithFormat:@"Cate %d", i];
            Category *cat = [[Category alloc] initWithContext:APPDELEGATE.persistentContainer.viewContext];
            cat.name = str;
            cat.iconname = @"cat_gift";
            [temp addObject:cat];
        }
        arrCate = [NSArray arrayWithArray:temp];
    }
    if(amount < 50) {
        return [arrCate subarrayWithRange:NSMakeRange(0, amount > 50?50:amount)];
    }
    return arrCate;
}

@end
