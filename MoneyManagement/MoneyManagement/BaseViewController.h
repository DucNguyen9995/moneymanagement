//
//  BaseViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/20/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@end
