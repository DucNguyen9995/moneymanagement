//
//  BalanceViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 12/26/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BaseViewController.h"

@interface BalanceViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableOverViewBalance;
@property (nonatomic, assign) CGFloat currentCash;
@property (nonatomic, strong) NSMutableArray<NSNumber *> *arrayCashOverTime;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSNumber *> *dictBanks;

@end
