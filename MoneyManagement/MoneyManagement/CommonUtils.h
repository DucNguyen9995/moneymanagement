//
//  CommonUtils.h
//  WebSourceViewerAndPlayer
//
//  Created by Duc Nguyen on 11/21/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

@interface CommonUtils : NSObject
+ (UIImage*)resizeImage:(UIImage*)image
             toFitWidth:(CGFloat)width
                 height:(CGFloat)height;
+ (void)fatalInClass:(NSString *)className
            atMethod:(NSString *)methodName
           whileTask:(NSString *)taskName;
+ (UIViewController *)topMostViewController;
@end


#pragma mark - This is for money Tracker only
#import "MoneyEvent+CoreDataClass.h"
#import "IncomeEvent+CoreDataClass.h"
#import "ExpenseEvent+CoreDataClass.h"
#import "Bank+CoreDataClass.h"
#import "BankDiary+CoreDataClass.h"
#import "Category+CoreDataClass.h"
#import "AppDelegate.h"
@interface CommonUtils (MoneyTracker)
+ (NSArray<ExpenseEvent *> *)fetchTestArrayExpense:(NSUInteger)amount;
+ (NSArray<IncomeEvent *> *)fetchTestArrayIncome:(NSUInteger)amount;
+ (NSArray<Bank *> *)fetchBanks;
+ (NSArray<BankDiary *> *)fetchBankDiaries:(NSUInteger)amount;
+ (NSArray<Category *> *)fetchCategories:(NSUInteger)amount;
+ (void)addIncomeEvent:(IncomeEvent *)evt;
+ (void)addExpenseEvent:(ExpenseEvent *)evt;
@end
