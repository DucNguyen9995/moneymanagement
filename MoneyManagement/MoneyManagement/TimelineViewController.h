//
//  TimelineViewController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/6/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "BaseViewController.h"
#import "MoneyEvent+CoreDataClass.h"
#import "TimeLineViewControl.h"

@interface TimelineViewController : BaseViewController

@property (nonatomic, strong) NSArray<MoneyEvent *> *moneyEvents;
@end
