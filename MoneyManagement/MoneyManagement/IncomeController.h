//
//  IncomeController.h
//  MoneyManagement
//
//  Created by Duc Nguyen on 1/8/17.
//  Copyright © 2017 Duc Nguyen. All rights reserved.
//

#import "MoneyController.h"
#import "IncomeEvent+CoreDataClass.h"

#define kEntityIncomeName                       @"IncomeEvent"

@interface IncomeController : MoneyController

+ (instancetype)sharedInstance;

- (BOOL)addEvent:(IncomeEvent *)evt;
- (NSArray<IncomeEvent *> *)getEventsFrom:(NSDate *)fromDate
                                       to:(NSDate *)toDate;
- (BOOL)deleteEvent:(IncomeEvent *)evt;

@end
